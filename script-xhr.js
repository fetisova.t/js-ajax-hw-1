/**
 * Created by fitki on 06.09.2019.
 */
const btn = document.querySelector('.main-button');
const container = document.querySelector('.content-wrap');
let characterTitle;
const movieList = document.createElement('div');
const requestMovies = new XMLHttpRequest();


requestMovies.open('GET', 'https://swapi.co/api/films/', true);

requestMovies.onreadystatechange = function () {
    if(requestMovies.readyState === 4 && requestMovies.status === 200){
        const result = JSON.parse(requestMovies.response);
        console.log(result);
        renderList(result.results);
    }
};


function renderList(list) {
        console.log('function renderList(list) started, list: ',list);
        movieList.classList.add('movie-list');

        list.forEach((elem)=>{
            console.log('function renderList, list.forEach - elem:',elem);
            const movieCard = document.createElement('div');
            movieCard.classList.add('movie-card');
            movieCard.innerHTML = `
            <span class="episode">0${elem.episode_id}</span>
            <h2 class="title">${elem.title}</h2>    
            <h3 class="characters-title">Characters:</h3>              
            <p class="opening-crawl">${elem.opening_crawl}</p>
    `;
            movieList.appendChild(movieCard);
            let childrenMovieCard = Array.from(movieCard.children);
            characterTitle = childrenMovieCard[2];
            console.log('movieCard.children[2] characterTitle: ',characterTitle);
            renderCharacter(elem.characters, characterTitle);
        });
    container.appendChild(movieList);
    }

function renderCharacter(characters, characterTitle) {
    const characterList = document.createElement('div');
    characterList.classList.add('character-list');
    characters.forEach((character)=>{
        const requestCharacters = new XMLHttpRequest();
        requestCharacters.open('GET', character, true);
        requestCharacters.onreadystatechange = function () {
            if(requestCharacters.readyState === 4 && requestCharacters.status === 200){
                const result = JSON.parse(requestCharacters.response);
                console.log('result в переборе characters: ',result);
                const characterItem = document.createElement('p');
                characterItem.classList.add('characters');
                characterItem.innerText = `${result.name}`;
                characterList.appendChild(characterItem);
                characterTitle.after(characterList);
            }
        };
        requestCharacters.send();

    });
}

btn.addEventListener('click', function (e){
    e.preventDefault();
    requestMovies.send();

});