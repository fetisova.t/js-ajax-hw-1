/**
 * Created by fitki on 06.09.2019.
 */

const btn = document.querySelector('.main-button');
const container = document.querySelector('.content-wrap');
let characterTitle;
const movieList = document.createElement('div');

async function fetchData(url) {
    const result = await fetch(url);
    const data = await result.json();
    let list = await data.results;
    await list.forEach((elem)=>{
            console.log('list.forEach - elem:',elem);
            const movieCard = document.createElement('div');
            movieCard.classList.add('movie-card');
            movieCard.dataset.id = elem.episode_id;
            movieCard.innerHTML = `
                <span class="episode">0${elem.episode_id}</span>
                <h2 class="title">${elem.title}</h2>
                <h3 class="characters-title" data-id="${elem.episode_id}">Characters:</h3>
                <p class="opening-crawl">${elem.opening_crawl}</p>
        `;
            movieList.appendChild(movieCard);

        let urls = elem.characters;
        console.log('urls',urls);
        let promiseArray = [];
        urls.forEach((character)=>{
            let fetchCharacter = fetch(character);
            promiseArray.push(fetchCharacter)
        });
        let answerArray = Promise.all(promiseArray)
            .then(responses=>{
                let responseArray = [];

                console.log('responses',responses);
                responses.forEach(response=>{
                        let parsedData= response.json();
                        console.log('parsed Data',parsedData);
                        responseArray.push(parsedData);
                    });
                    return responseArray
                })
            .then(results=>{
                let finalRes = Promise.all(results);
                finalRes.then(res=>{
                    const characterList = document.createElement('div');
                    characterList.classList.add('character-list');
                    res.forEach(resItem=>{
                        const characterItem = document.createElement('p');
                        characterItem.classList.add('characters');
                        characterItem.innerText = `${resItem.name}`;
                        characterList.appendChild(characterItem);
                    });
                    let childrenMovieCard = Array.from(document.querySelector(`.movie-card[data-id="${elem.episode_id}"]`).children);

                    characterTitle = childrenMovieCard[2];
                    console.log('movieCard.children[2] characterTitle: ',characterTitle);
                    characterTitle.after(characterList);
                })
            });

        });
    container.appendChild(movieList);
}
btn.addEventListener('click', function (e) {
    e.preventDefault();
    fetchData('https://swapi.co/api/films/');
});


